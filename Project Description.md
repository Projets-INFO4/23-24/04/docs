# Open World Lego

# Description

Welcome to the project for designing the foundations of an OpenWorld engine, harnessing the capabilities of the Godot 4 game engine and leveraging resources provided by the tool suite and LDraw format. This project aims to create a valuable and interactive virtual environment by combining the power of Godot 4 with detailed models available in the tool suite and the LDraw format.

## Installation and Requirements 

1. Install [Leocad](https://www.leocad.org/download.html) - Use to the conversion process
2. Clone our repository
3. Build using Godot

## Supported Version

- **Windows** : Supported
- **Linux** : Supported except for importing new content
- **MacOS** : Supported except for importing new content

## Global Architecture of the Godot project

![Global Architecture of the Godot project](./Specification/Architecture%20on%20Godot.png)

## Table of contents

- [LDraw format to Godot](./Specification/LDrawToGodot.md)

- [Game Specification](./Specification/GameSpecification.md)

- [Interface Specification](./Specification/InterfaceSpecification.md)

- [LDraw Convertissor](./Specification/LDrawConvertissor.md)

- [World Generation](./Specification/WorldGeneration.md)

- [Unresolved Bug Founded](./Specification/BugFounded.md)

- [Tasks to add to the game](./Specification/TasksToAdd.md)
