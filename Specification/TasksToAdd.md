# Tasks to add to the game
    This game needs more features and serveral existing features need some enhancements.

## Player
- Add life points
- Attack / Defence

## Enemy
- The shortest path algorithm to follow the player
- Life point system
- Attack / Defence

## World
- Change generation tree to adapt to the biomes
- Some details are available [here](WorldGeneration.md)
- Download the world when exit the game and reload it
- Upgrade inventory and craft

## Convertion LDraw
- Fix bug importation. [Here for more details](BugFounded.md)
- Add new Node type on uploading like MoveableEntity or Item
- Make the convertion process extern. (Create or Adapt a code that will convert)

## Inventory

- Only the data structure and the inventory user interface have been implemented. All the actions applicable to the inventory (store / release an object, select an object, etc.) should then be implemented and linked to the UI.
- Developing items

