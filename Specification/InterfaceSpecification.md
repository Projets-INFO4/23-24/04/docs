# Menu Functionality Explanation

The menu in our application serves as the central hub for accessing various features and settings. 
Each of the menu are build to be responsive by adding them to Container in Godot.
Below is a breakdown of how the menu operates:

1. **Main Menu**:
   - When the application starts, the main menu is displayed.
   - It provides options to navigate to the 3 next different sections.

2. **Game Custom Menu**:
   - The interface before creating a World
   - Menu is used to set the difficulty of the world
   - Menu is used to set seed of the world if wanted.

3. **Settings Menu**:
   - Can setting different of about the Graphics/Sounds/Accessibility/Control
  
4. **Credit View**:
   - Show of the Credit of the game

5. **Inventory**:
   - Show all the inventory of the player
   - Should be 4x9 places in the inventory

6. **In Game Gui**:
   - Show the toolbars of the player
   - Show the Health of the player
   - Show the Stamina of the player
  
![Menu Sketch](menu_sketch.jpg)