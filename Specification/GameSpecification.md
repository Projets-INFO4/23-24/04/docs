# Interaction Specification

## Mouse

- Left Click:
  - Hold: Destroy blocks
  - Click: Attack
- Mouse Wheel: Navigate the toolbar
- Middle Click: *Needs Specification*
- Right Click:
  - Place blocks
  - Use objects
  - Swing levers or doors
  - Shoot arrows
  - Interact with certain blocks (chests, workbenches, furnaces, etc.)
  - Push buttons

## Keyboard

- Z: Move forward
- Q: Move left
- S: Move backward
- D: Move right
- Space: Jump
- 1 to 9: Select item from toolbar
- I: Open inventory
- L: Open Import Menu
- E: Release current item
- Shift: Run
- Esc: Open game menu
